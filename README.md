# Artificial Dostoevsky
This project aims to fine-tune the ruGPT3 language model to generate text in the style of the famous Russian novelist Fyodor Dostoevsky. I provide a dataset for training the model.

## Dataset
The dataset I am using is a collection of Dostoevsky's works, including novels and short stories. I have parsed the data from Russian website lib.ru. I have cleaned and preprocessed the text data to make it suitable for training the language model.

## Fine-tuning ruGPT3
I use the Hugging Face Transformers library to fine-tune the ruGPT3 model on our Dostoevsky dataset. The resulting model will be able to generate text that resembles Dostoevsky's writing style.

## Usage
I provide scripts to download the dataset and fine-tune the language model on it. After fine-tuning, you can generate text in the style of Dostoevsky using the provided generation script.
